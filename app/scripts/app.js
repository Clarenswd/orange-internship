'use strict';

/**
 * @ngdoc overview
 * @name dogeWeatherApp
 * @description
 * # dogeWeatherApp
 *
 * Main module of the application.
 */
angular
  .module('dogeWeatherApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(['$routeProvider', '$locationProvider',function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact',{
        templateUrl:'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
      //needs more work on the following line for clean urls
    $locationProvider.html5Mode({enabled:true, requireBase : false});
  }]);
