'use strict';

/**
 * @ngdoc function
 * @name dogeWeatherApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dogeWeatherApp
 */

angular.module('dogeWeatherApp')
    .controller('MainCtrl', ['$scope', '$http',
        function ($scope, $http) {

            $scope.dogeImage = 'doge.jpg';

            //Grab geolocation
            $scope.getLoc = function () {
                try {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            $scope.$apply(function () {
                                $scope.position = position;
                                $scope.latitude = position.coords.latitude;
                                $scope.longitude = position.coords.longitude;
                                return true;

                            });
                        });
                    } else {
                        $scope.showError('Your browser does not support Geolocation!');
                        return false;
                    }
                    
                } catch (err) {
                    $scope.showError(err.message);
                }
            };





            $scope.convertToCelcius = function (temperature) {

                var fTempVal = parseFloat(temperature);
                var cTempVal = (fTempVal - 32) * (5 / 9);
                return Math.round(cTempVal * 100) / 100;

            };

            //Yahoo API
            $scope.clickWeather = function () {
                $scope.spinner = true;
                $scope.state = typeof $scope.state === 'undefined' ? 'QLD' : $scope.state;
                $scope.city = typeof $scope.city === 'undefined' ? 'Brisbane' : $scope.city;

                //                window.alert($scope.city);
                //                window.alert($scope.state);

                $http.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22' + $scope.city + ' ' + $scope.state + '%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys').
                success(function (data) {
                    //parse Data
                    //                    $scope.urlAPI = data;
                    if (data.query.count === 0) {
                        $scope.error = 'Error while processing the call';
                    } else {
                        $scope.temperature = $scope.convertToCelcius(data.query.results.channel.item.condition.temp);
                        $scope.description = data.query.results.channel.item.condition.text;
                        var desc = $scope.description.toUpperCase();

                        if (
                            (desc.indexOf('CLOUD') > -1) || 
                            (desc.indexOf('CLOUD') > -1) || 
                            (desc.indexOf('RAIN') > -1)  || 
                            (desc.indexOf('SHOWER') > -1)|| 
                            (desc.indexOf('STORM') > -1)
                        ) {
                            $scope.weatherBackground ={
                                background:'url(images/cloudy.jpg)'
                            };
                                
                            $scope.umbrellaMessage = 'will need your umbrella!';
                            $scope.dogeImage = 'doge_umbrella.jpg';
                        } else {
                            if ($scope.description.indexOf('sun') > -1) {
                                $scope.umbrellaMessage = 'need to wear sunnies!';
                                $scope.dogeImage = 'doge_sunny.jpg';
                            } else {
                                $scope.umbrellaMessage = 'don\'t need to take your umbrella!';
                            }
                        }

                        $scope.answer = false;
                        $scope.spinner = true;
                         


                    }

                }).
                error(function (data) {
                    // called asynchronously if an error occurs
                    $scope.error = 'Error ::' + data;
                });
            };

            //THE BEGINNING
            $scope.getLoc();
             
            //YahooAPI
            $scope.APPID = 'NkbZYd70'; // Your Yahoo Application ID
            $scope.DEG = 'c'; // c for celsius, f for fahrenheit
            $scope.answer = true;
            $scope.button = false;
            $scope.spinner = true;

             
            $scope.doSmt = function () {
                 
                 
                if (typeof $scope.longitude !== 'undefined') {
                    $scope.spinner = false;
                    //GoogleInvertedGeocoding
                    $http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + $scope.latitude + ',' + $scope.longitude + '&key=AIzaSyDTg_dXbaWr6d8nwh7hUjz1w31bfvTsg1Q')
                        .success(function (data) {
                            //Log the response from google 
                            //console.dir(data);
                            //                        console.log(data.results[1].address_components[0].long_name);
                            //                        console.log(data.results[1].address_components[1].short_name);
                            $scope.city = data.results[1].address_components[0].long_name;
                            $scope.state = data.results[1].address_components[1].short_name;
                            //Follwing line contains the Yahoo stuff
                            $scope.clickWeather();



                        })
                        .error(function (data) {
                            //Log the error first then try with the openWeatherMap... free version is innacurate though
                            console.log(data);
 
                        });

                } else {
                    window.alert('Sorry we are still detecting your location');
                    $scope.getLoc();
                }
            };




            $scope.showError = function (msg) {
                $scope.error = msg;
            };



                    }]);